module Main where

import Test.Hspec
import Data.PureArray.C

main :: IO ()
main = hspec $
    describe "Linear usage" $
        it "Initializes correctly" $ do
            let arr = newPureArray 0 100 :: PureArrayUnboxed Int
            arr ! 5 `shouldBe` 0
            arr ! 6 `shouldBe` 0
