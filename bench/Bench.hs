{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -fprof-auto -fprof-auto-calls #-}
module Main where

import Criterion.Main
import qualified Data.Vector.Unboxed.Mutable as V

import Data.PureArray.C

import Control.Monad
import Data.List (foldl')
import GHC.IO.Encoding


testSize = 100000

ioVectorCount :: IO Int
ioVectorCount = do
    v <- V.replicate testSize 0
    forM_ [1..testSize - 1] $ \i -> do
        !x <- V.read v (i - 1)
        let !s = x + 1 in V.write v i s
    V.read v (testSize - 1)

pureArrayCount :: IO Int
pureArrayCount =
    res `seq` return res
    where v' = foldl' (\v i -> write i ((v ! (i - 1)) + 1) v)
                      (newPureArray 0 testSize :: PureArrayBoxed Int)
                      [1..testSize - 1]
          res = v' ! (testSize - 1)

fwSize :: Num a => a
fwSize = 100

ioVectorFW :: IO Double
ioVectorFW = do
    v <- V.replicate (fwSize * fwSize) (0 :: Double)
    forM_ [0..fwSize * fwSize - 1] $ \i -> V.write v i (fromIntegral i / (fwSize * fwSize))
    forM_ [(i, j, k) | !i <- [0..fwSize - 1], !j <- [0..fwSize - 1], !k <- [0..fwSize - 1]]
        $ \(i, j, k) -> do
            !x <- V.read v (i * fwSize + j)
            !s <- (+) <$> V.read v (i * fwSize + k) <*> V.read v (k * fwSize + j)
            when (x > s) $ V.write v (i * fwSize + j) s
    foldM (\s i -> (s +) <$> V.read v (i * fwSize + i)) 0 [0..fwSize - 1]

pureArrayFW :: IO Double
pureArrayFW = do
    let !v = newPureArray (0 :: Double) (fwSize * fwSize) :: PureArrayUnboxed Double
    let !initial =
            foldl' (\v' i -> write i (fromIntegral i / (fwSize * fwSize)) v') v [0..fwSize * fwSize - 1]
    let !solved =
            for initial [(i, j, k) | !i <- [0..fwSize - 1], !j <- [0..fwSize - 1], !k <- [0..fwSize - 1]] $
                \ !v' !(i, j, k) -> do
                    let !x = v' ! (i * fwSize + j)
                        !s = v' ! (i * fwSize + k) + v' ! (k * fwSize + j)
                    if x > s then write (i * fwSize + j) s v' else v'
    let !res = foldl' (\s i -> s + solved ! (i * fwSize + i)) 0 [0..fwSize - 1]
    return res
    where for s ix f = foldl' f s ix
          {-# INLINE for #-}

main :: IO ()
main = do
    setLocaleEncoding utf8
    -- defaultMain
    --     [ bench "IOVector count" (whnfIO ioVectorCount)
    --     , bench "PureArray count" (whnfIO pureArrayCount)
    --     ]
    defaultMain
        [ bench "IOVector FW" (whnfIO ioVectorFW)
        , bench "PureArray FW" (whnfIO pureArrayFW)
        ]
    -- pureArrayCount >>= print
