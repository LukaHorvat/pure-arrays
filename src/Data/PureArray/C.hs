{-# LANGUAGE ForeignFunctionInterface, TypeFamilies, BangPatterns #-}
module Data.PureArray.C where

import Foreign.Ptr
import Foreign.StablePtr

import qualified Data.Vector.Mutable as B
import qualified Data.Vector.Unboxed.Mutable as U
import qualified Data.Vector.Generic.Mutable as V
import Control.Monad.ST (RealWorld)
import GHC.IO.Unsafe

data PureArrayC (vec :: * -> *) a
type PureArray (vec :: * -> *) a = Ptr (PureArrayC vec a)

foreign import ccall "wrapper" writeToFunPtr ::
       (Int -> StablePtr a -> StablePtr (vec a) -> IO ())
    -> IO (FunPtr (Int -> StablePtr a -> StablePtr (vec a) -> IO ()))
foreign import ccall "wrapper" readToFunPtr ::
       (Int -> StablePtr (vec a) -> IO (StablePtr a))
    -> IO (FunPtr (Int -> StablePtr (vec a) -> IO (StablePtr a)))
foreign import ccall "wrapper" cloneToFunPtr ::
       (StablePtr (vec a) -> IO (StablePtr (vec a)))
    -> IO (FunPtr (StablePtr (vec a) -> IO (StablePtr (vec a))))

foreign import ccall "array.c new_pure_array" newPureArrayC ::
    Int -> StablePtr (vec a) -> PureArray vec a
foreign import ccall "array.c index_arr" indexArrC ::
       Int -> PureArray vec a -> FunPtr (Int -> StablePtr (vec a) -> IO (StablePtr a))
    -> StablePtr a
foreign import ccall "array.c write_arr" writeArrC ::
       Int -> StablePtr a -> PureArray vec a
    -> FunPtr (Int -> StablePtr a -> StablePtr (vec a) -> IO ())
    -> FunPtr (StablePtr (vec a) -> IO (StablePtr (vec a)))
    -> FunPtr (Int -> StablePtr (vec a) -> IO (StablePtr a))
    -> PureArray vec a

ptrWrite :: (v RealWorld ~ vec, V.MVector v a) => Int -> StablePtr a -> StablePtr (vec a) -> IO ()
ptrWrite ix elPtr vecPtr = do
    el <- deRefStablePtr elPtr
    vec <- deRefStablePtr vecPtr
    V.write vec ix el

writeFunPtr :: (v RealWorld ~ vec, V.MVector v a)
            => FunPtr (Int -> StablePtr a -> StablePtr (vec a) -> IO ())
writeFunPtr = unsafePerformIO (writeToFunPtr ptrWrite)
{-# NOINLINE writeFunPtr #-}

ptrRead :: (v RealWorld ~ vec, V.MVector v a)
        => Int -> StablePtr (vec a) -> IO (StablePtr a)
ptrRead ix vecPtr = do
    vec <- deRefStablePtr vecPtr
    newStablePtr =<< V.read vec ix

readFunPtr :: (v RealWorld ~ vec, V.MVector v a)
           => FunPtr (Int -> StablePtr (vec a) -> IO (StablePtr a))
readFunPtr = unsafePerformIO (readToFunPtr ptrRead)
{-# NOINLINE readFunPtr #-}

ptrClone :: (v RealWorld ~ vec, V.MVector v a)
         => StablePtr (vec a) -> IO (StablePtr (vec a))
ptrClone vecPtr = newStablePtr =<< V.clone =<< deRefStablePtr vecPtr

cloneFunPtr :: (v RealWorld ~ vec, V.MVector v a)
            => FunPtr (StablePtr (vec a) -> IO (StablePtr (vec a)))
cloneFunPtr = unsafePerformIO (cloneToFunPtr ptrClone)
{-# NOINLINE cloneFunPtr #-}

newPureArray :: (v RealWorld ~ vec, V.MVector v a) => a -> Int -> PureArray vec a
newPureArray val size = unsafePerformIO $ do
    !v <- V.replicate size val
    vecPtr <- newStablePtr v
    return (newPureArrayC size vecPtr)

(!) :: (v RealWorld ~ vec, V.MVector v a) => PureArray vec a -> Int -> a
(!) vec ix = unsafePerformIO (deRefStablePtr (indexArrC ix vec readFunPtr))

write :: (v RealWorld ~ vec, V.MVector v a) => Int -> a -> PureArray vec a -> PureArray vec a
write !ix !x !arr = unsafePerformIO $ do
    !xPtr <- newStablePtr x
    return (writeArrC ix xPtr arr writeFunPtr cloneFunPtr readFunPtr)

type PureArrayBoxed a = PureArray B.IOVector a
type PureArrayUnboxed a = PureArray U.IOVector a
