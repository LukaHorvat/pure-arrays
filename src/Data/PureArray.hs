{-# LANGUAGE RecordWildCards, LambdaCase, TypeFamilies, BangPatterns #-}
{-# OPTIONS_GHC -fprof-auto-calls -fprof-auto #-}
module Data.PureArray where

import Data.IORef
import qualified Data.Vector.Mutable as B
import qualified Data.Vector.Unboxed.Mutable as U
import qualified Data.Vector.Generic.Mutable as V

import GHC.IO.Unsafe
import Control.Monad.ST (RealWorld)

data PureArrayRep vec a =
      Log { newValue ::                !a
          , index    :: {-# UNPACK #-} !Int
          , newArray :: {-# UNPACK #-} !(PureArray vec a) }

    | Head { vector :: !(vec a) }

data PureArray vec a = PureArray {-# UNPACK #-} !Int
                                 {-# UNPACK #-} !(IORef (PureArrayRep vec a))

fromRep :: Int -> PureArrayRep vec a -> IO (PureArray vec a)
fromRep size !rep = {-# SCC fromRep #-} PureArray size <$> newIORef rep
{-# INLINE fromRep #-}

newPureArray :: (v RealWorld ~ vec, V.MVector v a) => a -> Int -> PureArray vec a
newPureArray val size = unsafePerformIO $ do
    !v <- V.replicate size val
    fromRep size (Head v)
{-# NOINLINE newPureArray #-}

(!) :: (v RealWorld ~ vec, V.MVector v a) => PureArray vec a -> Int -> a
(!) (PureArray size ref) ix
    | ix >= size = error "Index out of bounds"
    | otherwise  = {-# SCC "!" #-} unsafePerformIO $ readIORef ref >>= \case
        Log{..}  -> return $ if ix == index then newValue else newArray ! ix
        Head{..} -> V.unsafeRead vector ix
{-# INLINE (!) #-}

toVector :: (v RealWorld ~ vec, V.MVector v a) => PureArray vec a -> IO (vec a)
toVector (PureArray _ ref) = {-# SCC toVector #-} readIORef ref >>= \case
    Log{..} -> do
        v <- toVector newArray
        V.unsafeWrite v index newValue
        return v
    Head{..} -> V.clone vector
{-# INLINABLE toVector #-}

write :: (v RealWorld ~ vec, V.MVector v a) => Int -> a -> PureArray vec a -> PureArray vec a
write !ix !x (PureArray size ref)
    | ix >= size = error "Index out of bounds"
    | otherwise  = {-# SCC write #-} unsafePerformIO $ do
        (newArr, oldVal) <- readIORef ref >>= \case
            h@(Head v) -> do
                oldVal <- V.unsafeRead v ix
                V.unsafeWrite v ix x
                !newArr <- fromRep size h
                return $! (newArr, oldVal)
            Log{..} -> do
                v <- toVector newArray
                V.unsafeWrite v index newValue
                oldVal <- V.unsafeRead v ix
                V.unsafeWrite v ix x
                newArr <- fromRep size (Head v)
                return $! (newArr, oldVal)
        writeIORef ref (Log oldVal ix newArr)
        return newArr
{-# INLINE write #-}

type PureArrayBoxed = PureArray B.IOVector
type PureArrayUnboxed = PureArray U.IOVector
