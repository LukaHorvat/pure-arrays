#include <stdlib.h>
#include <string.h>

typedef void* io_vector;
typedef void* elem;

struct pure_array;
typedef struct pure_array_log
{
    elem               new_value;
    int                index;
    struct pure_array* new_array;
} pure_array_log;

typedef struct pure_array_head
{
    io_vector vector;
} pure_array_head;

typedef union pure_array_rep_union
{
    pure_array_log  log;
    pure_array_head head;
} pure_array_rep_union;

typedef struct pure_array_rep
{
    int                  tag;
    pure_array_rep_union uni;
} pure_array_rep;

typedef struct pure_array
{
    int            size;
    pure_array_rep rep;
} pure_array;

pure_array* new_pure_array(int size, io_vector vector)
{
    pure_array* arr = (pure_array*)malloc(sizeof(pure_array));
    arr->size = size;
    arr->rep.tag = 0;
    arr->rep.uni.head.vector = vector;
    return arr;
}

elem index_arr(int idx, pure_array* arr, elem (*io_index)(int, io_vector))
{
    switch (arr->rep.tag)
    {
        case 0:
            return io_index(idx, arr->rep.uni.head.vector);

        case 1:
            if (arr->rep.uni.log.index == idx) return arr->rep.uni.log.new_value;
            else return index_arr(idx, arr->rep.uni.log.new_array, io_index);
    }
    exit(120);
}

io_vector to_vector(pure_array* arr,
                    void (*io_write)(int, elem, io_vector),
                    io_vector (*io_clone)(io_vector))
{
    switch (arr->rep.tag)
    {
        case 0:
            return io_clone(arr->rep.uni.head.vector);
        case 1:
        {
            io_vector vec = to_vector(arr->rep.uni.log.new_array, io_write, io_clone);
            io_write(arr->rep.uni.log.index, arr->rep.uni.log.new_value, vec);
            return vec;
        }
    }
    exit(121);
}

pure_array* write_arr(int idx, elem x, pure_array* arr,
                      void (*io_write)(int, elem, io_vector),
                      io_vector (*io_clone)(io_vector),
                      elem (*io_read)(int, io_vector))
{
    elem old_val;
    pure_array* new_arr = (pure_array*)malloc(sizeof(pure_array));
    switch (arr->rep.tag)
    {
        case 0:
        {
            old_val = io_read(idx, arr->rep.uni.head.vector);
            io_write(idx, x, arr->rep.uni.head.vector);
            memcpy(new_arr, arr, sizeof(pure_array));
            break;
        }
        case 1:
        {
            io_vector vec = to_vector(arr, io_write, io_clone);
            old_val = io_read(idx, vec);
            io_write(idx, x, vec);
            new_arr->size = arr->size;
            new_arr->rep.tag = 0;
            new_arr->rep.uni.head.vector = vec;
            break;
        }
        default:
            exit(122);
    }
    arr->rep.tag = 1;
    arr->rep.uni.log.index = idx;
    arr->rep.uni.log.new_value = old_val;
    arr->rep.uni.log.new_array = new_arr;
    return new_arr;
}

void free_arr(pure_array* arr)
{
    free(arr);
}
